import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { UsuarioModel } from 'src/app/core/models/usuario.model';
import { faPenAlt, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'usuario-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class UsuarioListarComponent implements OnInit {

  constructor(private service: UsuarioService) { }

  faPenAlt = faPenAlt
  faTrash = faTrash

  public usuarios: UsuarioModel[] = [];

  ngOnInit() {
    this.service.listarUsuarios()
    .subscribe((res: UsuarioModel[]= []) => {
      this.usuarios = res;
    });
  }

  remover(id: number) {
    this.service.removerUsuario(id);
  }
}
