import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { UsuarioModel } from './../../../core/models/usuario.model';

@Component({
  selector: 'usuario-atualizar',
  templateUrl: './atualizar.component.html',
  styleUrls: ['./atualizar.component.scss']
})

export class UsuarioAtualizarComponent implements OnInit {

  constructor(private route: ActivatedRoute, private service: UsuarioService) { }

  usuario: UsuarioModel;

  ngOnInit() {
    let usuarioId = +this.route.snapshot.paramMap.get("id");
    this.service.buscarUsuario(usuarioId)
      .subscribe((res: UsuarioModel) => {
        this.usuario = res;
      });
  }

  salvar() {
    this.service.atualizarUsuario(this.usuario);
  }
}