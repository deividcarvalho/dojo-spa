import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioListarComponent } from './usuario/pages/listar/listar.component';
import { UsuarioAdicionarComponent } from './usuario/pages/adicionar/adicionar.component';
import { UsuarioAtualizarComponent } from './usuario/pages/atualizar/atualizar.component';

const routes: Routes = [
  { path: 'usuario', component: UsuarioListarComponent },
  { path: 'usuario/create/', component: UsuarioAdicionarComponent },
  { path: 'usuario/edit/:id', component: UsuarioAtualizarComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
