import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuarioModel } from 'src/app/core/models/usuario.model';

@Injectable()
export class UsuarioService {

  constructor(private http: HttpClient) { }

  listarUsuarios(): Observable<Object> {
    return this.http.get("https://localhost:44376/usuarios");
  }

  buscarUsuario(id: number): Observable<Object>{
    return this.http.get(`https://localhost:44376/usuarios/${id}`);
  }

  adicionarUsuario(usuario: UsuarioModel ){
    this.http.post(`https://localhost:44376/usuarios/`, usuario);
  }

  atualizarUsuario(usuario: UsuarioModel){
    this.http.put(`https://localhost:44376/usuarios/`, usuario);
  }

  removerUsuario(id: number){
    this.http.delete(`https://localhost:44376/usuarios/${id}`);
  }
}
