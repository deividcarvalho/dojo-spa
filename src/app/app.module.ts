import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { UsuarioAdicionarComponent } from './usuario/pages/adicionar/adicionar.component';
import { UsuarioAtualizarComponent } from './usuario/pages/atualizar/atualizar.component';
import { UsuarioListarComponent } from './usuario/pages/listar/listar.component';
import { ContainerComponent } from './core/layout/container/container.component';
import { HeaderComponent } from './core/layout/header/header.component';
import { MenuComponent } from './core/layout/menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { UsuarioService } from './core/services/usuario.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    UsuarioAdicionarComponent,
    UsuarioAtualizarComponent,
    UsuarioListarComponent,
    ContainerComponent,
    HeaderComponent,
    MenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    HttpClientModule,
    NgxDatatableModule,
    FontAwesomeModule,
    FormsModule,
    MatProgressSpinnerModule
  ],
  providers: [
    UsuarioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
